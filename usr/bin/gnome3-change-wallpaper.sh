#!/bin/bash

USERNAME=$(whoami)

WALLPAPER_BASE=$(eval readlink -f ~${USERNAME}/Pictures/Wallpapers)
QUEUE_DIR="${WALLPAPER_BASE}/Queue"
DONE_DIR="${WALLPAPER_BASE}/Done"
CURRENT_PTR="${WALLPAPER_BASE}/Current"
#Initialize the DBUS ENV

SRC_PIC=""
DST_PIC=""

while [ -z "$DST_PIC" ]
do
    SRC_PIC=$(find "${QUEUE_DIR}" -regex ".*\.\(jpg\|png\|gif\|jpeg\)$" -print0 | shuf -z | head -n 1 -z)

    if [ -z "$SRC_PIC" ]
    then
        echo "Restore all done to queue"
        mv "${DONE_DIR}"/* "$QUEUE_DIR"
    else
        BASE_PATH=$(python -c "import os.path; print os.path.relpath('$SRC_PIC', '$QUEUE_DIR')")
        DST_PIC="${DONE_DIR}/${BASE_PATH}"
        DST_DIR=$(dirname "$DST_PIC")
        [ -d "$DST_DIR" ] || mkdir -vp "$DST_DIR"
        mv  "$SRC_PIC" "$DST_PIC"
        ln -sf "$DST_PIC" "$CURRENT_PTR"
    fi
done


for pid in $(pgrep gnome-session -u $(whoami))
do
    session=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/${pid}/environ)
    export $session
    dconf write /org/gnome/desktop/background/picture-uri "'file://$DST_PIC'"
    dconf write /org/gnome/desktop/screensaver/picture-uri "'file://$DST_PIC'"
done

